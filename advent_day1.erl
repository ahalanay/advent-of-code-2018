-module(advent_day1).
-export([main/0]).

main() ->
    {ok, Device} = file:open("input",[read]),
    L = read_integers(Device),
    L1=lists:reverse(L),
    io:fwrite("The Frequency is ~w~n", [lists:sum(L)]),
%    Part 2.
    Sol = solve_gen(L1,[],[],notfound),
    io:fwrite("The first repeated frequency is ~w~n",[Sol]).

% Read contents of file
read_integers(Device) -> 
    read_integers(Device,[]).

read_integers(Device, Acc) ->
    case io:fread(Device,[],"~d") of
        eof -> Acc;
        {ok, [N1]} -> read_integers(Device,[N1] ++ Acc);
        {error, What} ->
            io:format("io:fread error: ~w~n", [What]),
            read_integers(Device, Acc)
    end.

solve_gen(L1,L2,Aux,Rez) ->
        case Rez of
        notfound -> case L1 of 
                        [] -> solve_gen(lists:reverse(Aux),L2,[],notfound);
                        [X | T] -> case L2 of 
                                       [] -> solve_gen(T,[X | L2],[X | Aux],notfound);
                                       [Y | _] -> Z = X+Y,
                                                   case lists:member(Z, L2) of 
                                                       true -> solve_gen(L1,L2,[],Z);
                                                       false -> solve_gen(T,[Z | L2],[X | Aux],notfound)
                                                   end
                                   end
                    end;
        _ -> Rez
    end.
