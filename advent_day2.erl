-module(advent_day2).
-export([main/0]).

main() ->
    {ok, Device} = file:open("input2",[read]),
    S = read_strings(Device),
    L = lists:reverse(string:lexemes(S," ")),
%    io:fwrite("The read list is ~s~n",[lists:nth(8,L)]),
    Sol = finder(L,2)*finder(L,3),
    io:fwrite("The code is ~w~n",[Sol]),
    Sol2=string:reverse(solve2(L)),
    io:fwrite("The maching component is ~s~n",[Sol2]).

% Read contents of file
read_strings(Device) -> 
    read_strings(Device,[]).

read_strings(Device, Acc) ->
    case io:fread(Device,[],"~s") of
        eof -> Acc;
        {ok, [S]} -> read_strings(Device,[S] ++ "  " ++ Acc);
        {error, What} ->
            io:format("io:fread error: ~w~n", [What]),
            read_strings(Device, Acc)
    end.

occurrences(X,L) ->
    occurrences(X,L,0).

occurrences(X,L,Acc) ->
    case X of 
        [] -> 0;
        _ -> case L of 
                 [] -> Acc;
                 [Y | L1] -> case hd(X)==Y of
                                 true -> occurrences(X, L1, Acc+1);
                                 false -> occurrences(X, L1,Acc)
                             end
             end
    end.

finder(L,N) ->
    finder(L,N,0).

finder_aux(X,N) ->
    case X of 
        [] -> false;
        [Hd | Tail] -> case occurrences([Hd],X) > N of
                           true ->  finder_aux([Z || Z <- Tail, Z /= Hd],2);
                           false -> case occurrences([Hd],X) == N of
                                        true -> true;
                                        false -> finder_aux(Tail,N)
                                    end
                       end
    end.

finder(L,N,Acc) ->
    case L of 
        [] -> Acc;
        [X0 | L0] -> case finder_aux(X0,N) of
                         true -> finder(L0,N,Acc+1);
                         false -> finder(L0,N,Acc)
                     end
    end.

differ(L1,L2) ->
    differ(L1,L2,[]).

differ(L1,L2,Acc) -> 
    case string:length(L1) /= string:length(L2) of
        true -> [];
        false -> case {L1,L2} of 
                    {[],[]} -> Acc;
                    {[X | T1],[Y|T2]} -> case X == Y of 
                                             true -> differ(T1,T2,[X|Acc]);
                                             false -> differ(T1,T2, Acc)
                                         end
                end
    end.
solve2_aux(X,L) ->
    case L of 
        [] -> notfound;
        [Y|T] -> case string:length(differ(X,Y)) == (string:length(X) - 1) of
                     true -> differ(X,Y);
                     false -> solve2_aux(X,T)
                 end
    end.
solve2(L) ->
    case L of
        [] -> notfound;
        [X | T] -> case solve2_aux (X,T) == notfound of
                       false -> solve2_aux(X,T);
                       true -> solve2(T)
                   end
    end.

%solve(L1,L2) ->
    %solve(L1,L2,1).
%solve(L1,L2,Acc) ->
    %case L1 of 
        %[] -> case L2 of 
                  %[] -> Acc;
                  %[Y | T2] -> solve(Y, T2, Acc)
              %end;
        %[X | T1] -> 
